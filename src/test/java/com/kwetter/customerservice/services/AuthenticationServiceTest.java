//package com.kwetter.customerservice.services;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.google.common.collect.Iterables;
//import com.kwetter.customerservice.domain.UserCredentials;
//import com.kwetter.customerservice.domain.UserRegistrasion;
//import com.kwetter.customerservice.domain.enities.Role;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.event.annotation.BeforeTestClass;
//
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//@SpringBootTest
//public class AuthenticationServiceTest {
//
//    @Autowired
//    AuthenticationService authenticationService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    RoleService roleService;
//
//    @Test
//    void registerUser() throws JsonProcessingException {
//        generateDatabase();
//        int size = Iterables.size(userService.getAll());
//        authenticationService.register(new UserRegistrasion("TestU","TestU","testU@test.nl","12345678"));
//
//        assertThat(Iterables.size(userService.getAll()))
//                .isNotEqualTo(size);
//    }
//
//    @Test
//    void loginUser() throws JsonProcessingException {
//        generateDatabase();
//        UserCredentials userCredentials = new UserCredentials("testU2@test.nl", "12345678");
//        authenticationService.register(new UserRegistrasion("TestU2","TestU2",userCredentials.getEmail(),userCredentials.getPassword()));
//
//        ResponseEntity<String> responseEntity = authenticationService.login(userCredentials, null);
//
//        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
//    }
//
//    @Test
//    void loginBadUser() throws JsonProcessingException {
//        UserCredentials userCredentials = new UserCredentials("testU3@test.nl", "12345678");
//        authenticationService.register(new UserRegistrasion("TestU3","TestU3",userCredentials.getEmail(),userCredentials.getPassword()));
//
//        userCredentials.setPassword("87654321");
//
//        ResponseEntity<String> responseEntity = authenticationService.login(userCredentials, null);
//
//        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
//    }
//
//    private void generateDatabase() {
//        Role role = new Role(1L, "User", null);
//
//        roleService.addRole(role);
//    }
//}
