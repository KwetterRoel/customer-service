package com.kwetter.customerservice.domain;

import java.util.UUID;

public class UserExport {

    private Long id;
    private String email;
    private String username;
    private UUID imageUuid;

    public UserExport(Long id, String email, String username, UUID imageUuid) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.imageUuid = imageUuid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UUID getImageUuid() {
        return imageUuid;
    }

    public void setImageUuid(UUID imageUuid) {
        this.imageUuid = imageUuid;
    }
}
