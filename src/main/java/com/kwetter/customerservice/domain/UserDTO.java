package com.kwetter.customerservice.domain;

import com.kwetter.customerservice.domain.enities.Image;
import com.kwetter.customerservice.domain.enities.Role;

public class UserDTO {
    private Long id;
    private String email;
    private String name;
    private String biography;
    private String location;
    private String website;
    private Image image;
    private String username;
    private String password;
    private Role role;

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getBiography() {
        return biography;
    }

    public String getLocation() {
        return location;
    }

    public String getWebsite() {
        return website;
    }

    public Image getImage() {
        return image;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }
}
