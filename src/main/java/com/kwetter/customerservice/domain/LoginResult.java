package com.kwetter.customerservice.domain;

public enum LoginResult {
    SUCCESS("Success"),
    WRONG_PASSWORD("Wrong password!"),
    NO_ACCOUNT("Account not found!"),
    ACCOUNT_DISABLED("Your account has been disabled!");

    public final String label;

    private LoginResult(String label) {
        this.label = label;
    }
}
