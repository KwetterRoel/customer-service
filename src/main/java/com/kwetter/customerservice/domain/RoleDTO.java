package com.kwetter.customerservice.domain;

import com.kwetter.customerservice.domain.enities.Permission;
import java.util.List;

public class RoleDTO {
    private Long id;
    private String name;
    private List<Permission> permissions;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }
}
