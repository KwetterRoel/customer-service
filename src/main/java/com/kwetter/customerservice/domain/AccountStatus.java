package com.kwetter.customerservice.domain;

public enum AccountStatus {
    ACTIVATED,
    DISABLED
}
