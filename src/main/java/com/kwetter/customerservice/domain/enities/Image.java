package com.kwetter.customerservice.domain.enities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class Image {
    @Id
    @GeneratedValue
    private Long id;

    @JsonIgnore
    private String name;

    @JsonIgnore
    private String mimetype;

    @Lob
    @JsonIgnore
    private byte[] pic;

    @Column(unique = true, length = 16, nullable = false)
    private UUID uuid;


    public Image(){}

    public Image(String name, String mimetype, byte[] pic, UUID uuid){
        this.name = name;
        this.mimetype = mimetype;
        this.pic = pic;
        this.uuid = uuid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public byte[] getPic() {
        return pic;
    }

    public void setPic(byte[] pic) {
        this.pic = pic;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
