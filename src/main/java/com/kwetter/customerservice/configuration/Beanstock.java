package com.kwetter.customerservice.configuration;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;


@Component
public class Beanstock {
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public Queue newUserQueue() {
        return new Queue("CustomerService_NewUser", true);
    }

    @Bean
    public Queue updateUserQueue() {
        return new Queue("CustomerService_UpdateUser", true);
    }

    @Bean
    public Queue newUserQueueToFollow() {
        return new Queue("CustomerService_NewUser_to_follow", true);
    }

    @Bean
    public Queue updateUserQueueToFollow() {
        return new Queue("CustomerService_UpdateUser_to_follow", true);
    }

}
