package com.kwetter.customerservice.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kwetter.customerservice.domain.RoleDTO;
import com.kwetter.customerservice.domain.UserDTO;
import com.kwetter.customerservice.domain.enities.User;
import com.kwetter.customerservice.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;


@RestController
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("hasAnyAuthority('CAN_GET_USERS')")
    @GetMapping("/users")
    public Iterable<User> getAll(){
        return userService.getAll();
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/users/{id}")
    public ResponseEntity<User> getProfile(@PathVariable Long id) {
        return userService.getProfile(id);
    }

    @PreAuthorize("isAuthenticated()")
    @PutMapping("/users/self")
    public ResponseEntity<String> updateProfile(@ApiIgnore Authentication authentication, @RequestBody UserDTO updatedUser)  throws JsonProcessingException {
        return userService.updateProfile(authentication, updatedUser);
    }

    @PreAuthorize("hasAnyAuthority('CAN_CHANGE_ROLES')")
    @PutMapping("/users/{userId}/role")
    public ResponseEntity<String> updateUserRole(@PathVariable Long userId, @RequestBody RoleDTO role) {
        return userService.updateUserRole(userId, role);
    }
}
