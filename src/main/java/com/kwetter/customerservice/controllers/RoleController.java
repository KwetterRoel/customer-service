package com.kwetter.customerservice.controllers;

import com.kwetter.customerservice.domain.RoleDTO;
import com.kwetter.customerservice.domain.enities.Role;
import com.kwetter.customerservice.services.RoleService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class RoleController {

    private RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PreAuthorize("hasAnyAuthority('CAN_GET_ROLES')")
    @GetMapping("/roles")
    public Iterable<Role> getAll(){
        return roleService.getAll();
    }

    @PreAuthorize("hasAnyAuthority('CAN_GET_ROLES')")
    @GetMapping("/roles/{id}")
    public Optional<Role> getById(@PathVariable Long id){
        return roleService.getById(id);
    }

    @PreAuthorize("hasAnyAuthority('CAN_MANAGE_ROLES')")
    @PostMapping("/roles")
    public ResponseEntity<String> addRole(@RequestBody RoleDTO role) {
        return roleService.addRole(new Role(role.getId(), role.getName(), role.getPermissions()));
    }

    @PreAuthorize("hasAnyAuthority('CAN_MANAGE_ROLES')")
    @PutMapping("/roles/{id}")
    public ResponseEntity<String> updateRole(@PathVariable Long id, @RequestBody RoleDTO role) {
        return roleService.updateRole(id, role);
    }

    @PreAuthorize("hasAnyAuthority('CAN_DELETE_ROLES')")
    @DeleteMapping("/roles/{id}")
    public void deleteById(@PathVariable Long id){
        roleService.deleteById(id);
    }

}
