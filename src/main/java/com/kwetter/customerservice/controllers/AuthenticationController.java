package com.kwetter.customerservice.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kwetter.customerservice.domain.*;
import com.kwetter.customerservice.services.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
public class AuthenticationController {

    private AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;

    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody UserRegistrasion user) throws JsonProcessingException {
        return authenticationService.register(user);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody UserCredentials credentials, HttpServletResponse response){
        return authenticationService.login(credentials, response);
    }

}

