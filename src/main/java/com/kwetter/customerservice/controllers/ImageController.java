package com.kwetter.customerservice.controllers;

import com.kwetter.customerservice.services.ImageService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

@RestController
public class ImageController {

    private ImageService imageService;

    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/file/image")
    public ResponseEntity<String> uploadMultipartFile(@ApiIgnore Authentication authentication, @RequestParam("file") MultipartFile file) {
       return imageService.uploadMultipartFile(authentication, file);
    }

    @GetMapping("/file/image/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable String id) {
        return imageService.getFile(id);
    }
}
