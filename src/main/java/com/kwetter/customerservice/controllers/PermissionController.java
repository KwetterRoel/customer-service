package com.kwetter.customerservice.controllers;

import com.kwetter.customerservice.domain.enities.Permission;
import com.kwetter.customerservice.services.PermissionService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PermissionController {

    private PermissionService permissionService;

    public PermissionController(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @PreAuthorize("hasAnyAuthority('CAN_GET_ROLES')")
    @GetMapping("/permissions")
    public Iterable<Permission> getAll(){
        return permissionService.getAll();
    }
}
