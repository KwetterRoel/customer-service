package com.kwetter.customerservice.repositories;

import com.kwetter.customerservice.domain.enities.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByEmail(String email);
}
