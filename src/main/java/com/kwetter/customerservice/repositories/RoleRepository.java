package com.kwetter.customerservice.repositories;

import com.kwetter.customerservice.domain.enities.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role, Long> {
    Optional<Role> findByName(String name);
}
