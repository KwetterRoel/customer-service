package com.kwetter.customerservice.repositories;

import com.kwetter.customerservice.domain.enities.Permission;
import org.springframework.data.repository.CrudRepository;

public interface PermissionRepository extends CrudRepository<Permission, Long> {
}
