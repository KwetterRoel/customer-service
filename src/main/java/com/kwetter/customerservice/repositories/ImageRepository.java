package com.kwetter.customerservice.repositories;

import com.kwetter.customerservice.domain.enities.Image;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface ImageRepository extends CrudRepository<Image, Long> {
    Optional<Image> findByUuid(UUID uuid);
}
