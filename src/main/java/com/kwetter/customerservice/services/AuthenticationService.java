package com.kwetter.customerservice.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kwetter.customerservice.domain.*;
import com.kwetter.customerservice.domain.enities.Permission;
import com.kwetter.customerservice.domain.enities.Role;
import com.kwetter.customerservice.domain.enities.User;
import com.kwetter.customerservice.repositories.RoleRepository;
import com.kwetter.customerservice.repositories.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AuthenticationService {

    @Value("${security.jwt.token.issuer}")
    private String ISSUER;

    @Value("${security.jwt.token.secret-key}")
    private String SECRET;

    @Value("${security.jwt.token.expire-length}")
    private long EXPIRATION; //in ms

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private RabbitTemplate rabbitTemplate;

    public AuthenticationService(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder, RabbitTemplate rabbitTemplate) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.rabbitTemplate = rabbitTemplate;
    }

    public ResponseEntity<String> register(UserRegistrasion user) throws JsonProcessingException {
        Optional<Role> userRole = roleRepository.findByName("User");
        if (userRole.isPresent()){
            User newUser = new User();
            newUser.setEmail(user.getEmail());
            newUser.setName(user.getName());
            newUser.setUsername(user.getUsername());
            newUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            newUser.setRole(userRole.get());
            newUser.setStatus(AccountStatus.ACTIVATED);
            User returnEntity = userRepository.save(newUser);
            rabbitTemplate.convertAndSend("CustomerService_NewUser", new ObjectMapper().writeValueAsString(new UserExport(returnEntity.getId(), returnEntity.getEmail(), returnEntity.getUsername(), null)));
            rabbitTemplate.convertAndSend("CustomerService_NewUser_to_follow", new ObjectMapper().writeValueAsString(new UserExport(returnEntity.getId(), returnEntity.getEmail(), returnEntity.getUsername(), null)));
            return ResponseEntity.ok().body("200");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No role available");
    }

    public ResponseEntity<String> login(UserCredentials credentials, HttpServletResponse response){
        Optional<User> optionalAccount = userRepository.findByEmail(credentials.email);
        // check if account exists
        if(optionalAccount.isPresent()){
            User user = optionalAccount.get();

            //Successful login
            if(bCryptPasswordEncoder.matches(credentials.password, user.getPassword()) && user.getStatus().equals(AccountStatus.ACTIVATED)){
                List<String> claims = user.getRole().getPermissions().stream().map(Permission::getName).collect(Collectors.toList());
                //generate the token
                String token = generateToken(EXPIRATION, user, claims);
                return ResponseEntity.ok().body(token);
            }
            //Incorrect password
            else if(user.getStatus().equals(AccountStatus.ACTIVATED)){

                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(LoginResult.WRONG_PASSWORD.label);
            }
            //Account has been disabled
            else if(user.getStatus().equals(AccountStatus.DISABLED)){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(LoginResult.ACCOUNT_DISABLED.label);
            }
        }

        //Account does not exist
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(LoginResult.NO_ACCOUNT.label);
    }

    private String generateToken(Long expiration, User user, List<String> authorities){
        Claims claims = Jwts.claims()
                .setIssuer(ISSUER)
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + expiration))
                .setSubject(user.getEmail());

        //Create token
        String token = Jwts.builder()
                .addClaims(claims)
                .claim("userId", user.getId())
                .claim("name", user.getUsername())
                .claim("role", user.getRole().getName())
                .claim("authorities", authorities)
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();

        return token;
    }
}
