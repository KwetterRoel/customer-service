package com.kwetter.customerservice.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kwetter.customerservice.domain.RoleDTO;
import com.kwetter.customerservice.domain.UserDTO;
import com.kwetter.customerservice.domain.UserExport;
import com.kwetter.customerservice.domain.enities.Role;
import com.kwetter.customerservice.domain.enities.User;
import com.kwetter.customerservice.repositories.RoleRepository;
import com.kwetter.customerservice.repositories.UserRepository;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private RabbitTemplate rabbitTemplate;

    public UserService(UserRepository userRepository, RoleRepository roleRepository, RabbitTemplate rabbitTemplate) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.rabbitTemplate = rabbitTemplate;
    }

    public Iterable<User> getAll() {
        return userRepository.findAll();
    }

    public ResponseEntity<User> getProfile(Long id) {
        Optional<User> optionalAccount = userRepository.findById(id);

        // check if account exists and return
        return optionalAccount.map(user -> ResponseEntity.ok().body(user)).orElseGet(() -> ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null));
    }

    public ResponseEntity<String> updateProfile(Authentication authentication, UserDTO updatedUser)  throws JsonProcessingException {
        Optional<User> optionalAccount = userRepository.findByEmail(authentication.getName());

        if (optionalAccount.isPresent()) {
            //set user information
            User user = optionalAccount.get();

            //set old email, username for check changed
            String oldEmail = user.getEmail();
            String oldUsername =  user.getUsername();

            user.setName(updatedUser.getName());
            user.setUsername(updatedUser.getUsername());
            user.setEmail(updatedUser.getEmail());
            user.setWebsite(updatedUser.getWebsite());
            user.setLocation(updatedUser.getLocation());
            user.setBiography(updatedUser.getBiography());

            User returnEntity = userRepository.save(user);


            if (!oldEmail.equals(user.getEmail()) || !oldUsername.equals(user.getUsername())) {
                rabbitTemplate.convertAndSend("CustomerService_UpdateUser", new ObjectMapper().writeValueAsString(new UserExport(returnEntity.getId(), returnEntity.getEmail(), returnEntity.getUsername(), returnEntity.getImage().getUuid())));
                rabbitTemplate.convertAndSend("CustomerService_UpdateUser_to_follow", new ObjectMapper().writeValueAsString(new UserExport(returnEntity.getId(), returnEntity.getEmail(), returnEntity.getUsername(), returnEntity.getImage().getUuid())));
            }
            //success
            return ResponseEntity.ok().body("200");
        }
        //profile not found
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    public ResponseEntity<String> updateUserRole(Long userId, RoleDTO role) {
        Optional<User> optionalAccount = userRepository.findById(userId);

        if (optionalAccount.isPresent()) {
            User user = optionalAccount.get();
            Optional<Role> optionalRole = roleRepository.findById(role.getId());

            if (optionalRole.isPresent()) {
                user.setRole(optionalRole.get());
                userRepository.save(user);
                //success
                return ResponseEntity.ok().body("200");
            }
            //role not found
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Role not exist");
        }
        //user not found
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User not exist");
    }
}
