package com.kwetter.customerservice.services;

import com.kwetter.customerservice.domain.RoleDTO;
import com.kwetter.customerservice.domain.enities.Role;
import com.kwetter.customerservice.repositories.RoleRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleService {
    private RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Iterable<Role> getAll(){
        return roleRepository.findAll();
    }

    public Optional<Role> getById(Long id){
        return roleRepository.findById(id);
    }

    public ResponseEntity<String> addRole(Role role) {
        roleRepository.save(role);
        return ResponseEntity.ok().body("200");
    }

    public ResponseEntity<String> updateRole(Long id, RoleDTO role) {
        Optional<Role> optionalRole = roleRepository.findById(id);

        if (optionalRole.isPresent()) {
            Role updateRole = optionalRole.get();
            updateRole.setName(role.getName());
            updateRole.setPermissions(role.getPermissions());
            roleRepository.save(updateRole);
            return ResponseEntity.ok().body("200");
        }
        //role not found
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Role not exist");
    }

    public void deleteById(Long id){
        roleRepository.deleteById(id);
    }
}
