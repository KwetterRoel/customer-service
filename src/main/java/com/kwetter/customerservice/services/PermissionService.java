package com.kwetter.customerservice.services;

import com.kwetter.customerservice.domain.enities.Permission;
import com.kwetter.customerservice.repositories.PermissionRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

@Service
public class PermissionService {
    private PermissionRepository permissionRepository;

    public PermissionService(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    public Iterable<Permission> getAll(){
        return permissionRepository.findAll();
    }
}
