package com.kwetter.customerservice.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kwetter.customerservice.domain.UserExport;
import com.kwetter.customerservice.domain.enities.Image;
import com.kwetter.customerservice.domain.enities.User;
import com.kwetter.customerservice.repositories.ImageRepository;
import com.kwetter.customerservice.repositories.UserRepository;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Service
public class ImageService {
    private ImageRepository imageRepository;
    private UserRepository userRepository;
    private RabbitTemplate rabbitTemplate;

    public ImageService(ImageRepository imageRepository, UserRepository userRepository, RabbitTemplate rabbitTemplate) {
        this.imageRepository = imageRepository;
        this.userRepository = userRepository;
        this.rabbitTemplate = rabbitTemplate;
    }

    public ResponseEntity<String> uploadMultipartFile(Authentication authentication, MultipartFile file) {
        Optional<User> optionalAccount = userRepository.findByEmail(authentication.getName());

        if (optionalAccount.isPresent()) {
            User user = optionalAccount.get();

            if (user.getImage() != null) {
                try {
                    user.getImage().setName(file.getOriginalFilename());
                    user.getImage().setMimetype(file.getContentType());
                    user.getImage().setPic(file.getBytes());
                    userRepository.save(user);
                    return ResponseEntity.ok().body("200");
                } catch (IOException e) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("FAIL! Maybe You had uploaded the file before or the file's size > 500KB");
                }
            } else {
                try {
                    // save file
                    Image imageFile = new Image(file.getOriginalFilename(), file.getContentType(), file.getBytes(), UUID.randomUUID());
                    user.setImage(imageFile);
                    User returnEntity = userRepository.save(user);

                    rabbitTemplate.convertAndSend("CustomerService_UpdateUser", new ObjectMapper().writeValueAsString(new UserExport(returnEntity.getId(), returnEntity.getEmail(), returnEntity.getUsername(), returnEntity.getImage().getUuid())));
                    rabbitTemplate.convertAndSend("CustomerService_UpdateUser_to_follow", new ObjectMapper().writeValueAsString(new UserExport(returnEntity.getId(), returnEntity.getEmail(), returnEntity.getUsername(), returnEntity.getImage().getUuid())));

                    return ResponseEntity.ok().body("200");
                } catch (Exception e) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("FAIL! Maybe You had uploaded the file before or the file's size > 500KB");
                }
            }
        }
        //profile not found
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    public ResponseEntity<byte[]> getFile(String id) {
        Optional<Image> fileOptional = imageRepository.findByUuid(UUID.fromString(id));

        if (fileOptional.isPresent()) {
            Image file = fileOptional.get();
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"").body(file.getPic());
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }
}
