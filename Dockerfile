#
# Build stage
#
FROM maven:3.6.0-jdk-11-slim AS build
COPY . .
RUN chmod +x install-libs.bat
RUN ./install-libs.bat
RUN mvn dependency:go-offline
RUN mvn clean package
#
# Package stage
#
FROM openjdk:8-jre-alpine
ARG JAR_FILE=target/*.jar
COPY --from=build ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]